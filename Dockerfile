# ColorMap Amazon ECS Worker

FROM python:3.6
MAINTAINER Matt Strautmann <matt.strautmann@gmail.com>

RUN apt-get update && \
        apt-get install -y \
        build-essential \
        cmake \
        git \
        wget \
        unzip \
        yasm \
        pkg-config \
        libswscale-dev \
        libtbb2 \
        libtbb-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libavformat-dev \
        libpq-dev

RUN python -m pip install numpy
RUN python -m pip install scipy
RUN python -m pip install pillow
RUN python -m pip install scikit-image
RUN python -m pip install sklearn
RUN python -m pip install urllib3
RUN python -m pip install requests
RUN python -m pip install opencv-python 

RUN \
  mkdir /src && \
  cd /src && \
  git clone https://bitbucket.org/mattstrautmann/colormap.git && \
  cd colormap

RUN chmod +x /src/colormap/colorJob.sh

WORKDIR /

ENTRYPOINT [ "src/colormap/colorJob.sh" ]