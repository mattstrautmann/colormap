import math
import requests 
import json
import numpy as np
import traceback as tb
import os, io, sys, csv

from PIL import Image
from io import BytesIO
from time import sleep    
from datetime import datetime
from urllib.request import urlopen
from mapmycolordetector import colorMap, kdTree


def validate(combined_hsv_arrays, input_hsi):
    vv = []
    for idx, [h,s,i] in enumerate(combined_hsv_arrays):
        hh = ( min(abs(h-input_hsi[0]), 360-abs(h-input_hsi[0])) )/180.
        ss = ( abs(s-input_hsi[1]) )/100.
        ii = ( abs(i-input_hsi[2]) )/100.
        vv.append([np.linalg.norm(np.array([hh,ss,ii])), idx])
    vvs = sorted(vv, key=lambda x: x[0])
    return vvs[0][1]
    
def main():
    # Download color mapping palette
    try:
        with open('./color_palette_hsv.tsv','r') as tsvin:
            cc = csv.reader(tsvin, delimiter='\t')

            H = []
            S = []
            I = []
            colors = []
            hex_colors = []

            for row in cc:
                colors.append(row[0])
                H.append( float(row[1]))        
                S.append( float(row[2]))
                I.append( float(row[3]))
                hex_colors.append(row[4])

            combined_arrays = np.stack([H,S,I]).transpose()
    except:
        print("Colormap import failure")

    try:       
        url = 'https://shoplookio-api-production.herokuapp.com/api/internal/product-colors/?page_size=100&format=json&color=black'    
        x = 0

        while 1:
            try:
                response = requests.get(url).json()
            except:
                print("Failed to grab list of urls...")
                sleep(300)
                pass
            items_to_classify = []
            items_to_classify.extend([(x['id'],x['s3img']) for x in response['results']])

            saved_colors = []
            for item in items_to_classify:
                try:        
                    # Grab file for processing 
                    fd = requests.get(item[1])
                    imgIO = BytesIO()
                    imgIO = BytesIO(fd.content)
                    f = Image.open(imgIO)
                    print("Importing file ", item[1])
                     # Color Detect
                    input_hsi = colorMap(f)
#                    dist, indexes = kdTree(combined_arrays,input_hsi)
#                    idx = int(indexes[0][0]
                    idx = validate(combined_arrays, input_hsi) 
                    saved_colors.append([ item[0], colors[idx]])
                    print("Found color: {}".format(colors[idx]))  
                except:
                    print('No color found....')
                    print()
                    tb.print_exc()
                    
            data = {"color_map": saved_colors}
            data = json.dumps(data)
            print(data)
            headers = {'Content-type': 'application/json', 'Cache-Control': 'no-cache'}
            url_out = 'https://shoplookio-api-production.herokuapp.com/api/internal/product-colors/?format=json'  
            output = requests.post(url_out, data=data, headers=headers)
            print(output.json())
    except:
        tb.print_exc()
        pass  

if __name__ == "__main__":
    main()
