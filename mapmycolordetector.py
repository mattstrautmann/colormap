import math
import numpy as np
from scipy import spatial

from colorthief import ColorThief
from colortranslate import rgb2hsv

from sklearn.neighbors import KDTree

def kdTree(combined_arrays, point):
	# Calculated KNN nearest neighbor
    # to add pickled model
    # s = pickle.dumps(tree)              
    # tree_copy = pickle.loads(s)                
    # dist, ind = tree_copy.query(X[0], k=3)         
    
    tree = KDTree(combined_arrays, leaf_size=1)   
    dist, ind = tree.query([point], k=1)
    return dist, ind

def colorMap(f):
    color_thief = ColorThief(f)
    colors = color_thief.get_color(quality=2) # Returning top 5 colors with high quality
    input_hsis = []
#    input_rgbs = []
    for color in colors:
#        if len(set(color)) == 1 and color[0] < 25:
#            continue
        r, g, b = color
       # print('r,g,b',r,g,b)
#    input_rgbs.append([r,g,b])
        h,s,i = rgb2hsv(r,g,b)
        s = s*100
        i = i*100
#    input_hsi = np.asarray([h,s,i])
        input_hsis.append(np.asarray([h,s,i]))
    return input_hsis[0]
