#!/bin/bash

date
echo "Args: $@"
env
echo "Starting color detect pipeline."
python /src/colormap/detectpipeline.py
echo "Color detect pipeline end...."
